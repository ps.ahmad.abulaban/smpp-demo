### References

* https://camel.apache.org/components/latest/smpp-component.html
* https://medium.com/nerd-for-tech/exposing-rest-endpoint-to-send-sms-using-apache-camel-springboot-and-smpp-protocol-7f0ef53a5a55
* Simulator: https://github.com/bilalahmed54/SMPPSIM-Selenium-Tool


### How to use

##### Sending through SMPP

1. Run the simulator 
2. Use postman to post message over http://localhost:8085/sms/sender

```json
{
	"id":1,
	"sourceAddr":"1234",
	"sourceAddrTon":1,
	"sourceAddrNpi":3,
	"destAddr":"5678",
	"destAddrTon":4,
	"destAddrNpi":6,
	"messageBody":"test sending"
}
```
You will notice a DeliveryReceipt message received in this case


##### Receiving through SMPP

1. Run the simulator 
2. Open http://localhost:88/inject_mo.htm in browser
3. Fill the short_message, source_addr, destination_addr ... fields (ensure that data_coding = 8 to support both english & arabic messages)
4. You will notice a DeliverSm message received in this case