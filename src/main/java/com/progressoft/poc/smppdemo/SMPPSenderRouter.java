package com.progressoft.poc.smppdemo;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SMPPSenderRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("seda:newMessage?concurrentConsumers=20").routeId("smpp-sender")
                .setHeader("CamelSmppSourceAddr", simple("${in.body.sourceAddr}"))
                .setHeader("CamelSmppSourceAddrTon", simple("${in.body.sourceAddrTon}"))
                .setHeader("CamelSmppSourceAddrNpi", simple("${in.body.sourceAddrNpi}"))
                .setHeader("CamelSmppDestAddr", simple("${in.body.destAddr}"))
                .setHeader("CamelSmppDestAddrTon", simple("${in.body.destAddrTon}"))
                .setHeader("CamelSmppDestAddrNpi", simple("${in.body.destAddrNpi}"))
                .setBody(simple("${in.body.messageBody}"))
                .to("smpp://{{smpp.tr.systemid}}@{{smpp.tr.host}}:{{smpp.tr.port}}?password={{smpp.tr.password}}&enquireLinkTimer=3000&transactionTimer=5000&encoding=UTF-8&dataCoding=8");

    }
}