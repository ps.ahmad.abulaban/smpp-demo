package com.progressoft.poc.smppdemo;

import lombok.Data;

@Data
public class Message {

    private int id;
    private String sourceAddr;
    private int sourceAddrTon;
    private int sourceAddrNpi;
    private String destAddr;
    private int destAddrTon;
    private int destAddrNpi;
    private String messageBody;
}
