package com.progressoft.poc.smppdemo;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.smpp.SmppMessage;
import org.jsmpp.bean.AbstractSmCommand;
import org.springframework.stereotype.Component;

@Component
public class SMPPReceiverRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("smpp://{{smpp.tr.systemid}}@{{smpp.tr.host}}:{{smpp.tr.port}}?password={{smpp.tr.password}}&enquireLinkTimer=3000&transactionTimer=5000&systemType=consumer&encoding=UTF-16")
                .routeId("smpp-receiver")
                .process(this::process);
    }

    private void process(Exchange exchange) {
        String camelSmppMessageType = (String) exchange.getIn().getHeader("CamelSmppMessageType");
        SmppMessage message = exchange.getIn(SmppMessage.class);
        AbstractSmCommand command = (AbstractSmCommand) message.getCommand();
        System.out.println("------Receiving Message------" +
                "\nCamelSmppMessageType : " + camelSmppMessageType +
                "\nBody : " + message.getBody() +
                "\nSource Addr : " + command.getSourceAddr() + " , Source Addr Ton : " + command.getSourceAddrTon() + ", Source Addr Npi : " + command.getSourceAddrNpi() +
                "\nDest Addr : " + command.getDestAddress() + " , Dest Addr Ton : " + command.getDestAddrTon() + ", Dest Addr Npi : " + command.getDestAddrNpi() +
                "\n------End Receiving Message------");
    }
}
