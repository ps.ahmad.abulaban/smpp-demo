package com.progressoft.poc.smppdemo;

import lombok.AllArgsConstructor;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.engine.DefaultProducerTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping("/sms/sender")
public class SMSController {

    private final CamelContext camelContext;

    @PostMapping
    public ResponseEntity post(@RequestBody Message message) {
        System.out.println("SMSController post : " + message);
        sendToRoute(message);
        return ResponseEntity.ok().build();
    }

    private void sendToRoute(Message message) {
        try {
            ProducerTemplate template = new DefaultProducerTemplate(camelContext);
            template.start();
            template.sendBody("seda:newMessage", message);
            template.stop();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
